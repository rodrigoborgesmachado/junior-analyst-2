# Solução proposta:

## Considerações Gerais

* Sua solução deverá ser desenvolvida em dotnet core 2.1+. 
* Colocação: A solução foi desenvolvida com dotnet 3.1.401

* No seu README, você deverá fazer uma explicação sobre a solução encontrada, tecnologias envolvidas e instrução de uso da solução. 
* Colocação: Feito

* É interessante que você também registre ideias que gostaria de implementar caso tivesse mais tempo.
* Colocação: Final do arquivo

## Problema

Imagine que você ficou responsável por construir um sistema que seja capaz de receber milhares de eventos por segundo de sensores espalhados pelo Brasil, nas regiões norte, nordeste, sudeste e sul. Seu cliente também deseja que na solução ele possa visualizar esses eventos de forma clara.

Um evento é defino por um JSON com o seguinte formato:

```json
{
   "timestamp": <Unix Timestamp ex: 1539112021301>,
   "tag": "<string separada por '.' ex: brasil.sudeste.sensor01 >",
   "valor" : "<string>"
}
```

Descrição:
 * O campo timestamp é quando o evento ocorreu em UNIX Timestamp.
 * Tag é o identificador do evento, sendo composto de pais.região.nome_sensor.
 * Valor é o dado coletado de um determinado sensor (podendo ser numérico ou string).

## Requisitos

* Sua solução deverá ser capaz de armazenar os eventos recebidos.

* Cada sensor envia um evento a cada segundo independente se seu valor foi alterado, então um sensor pode enviar um evento com o mesmo valor do segundo anterior.

* Cada evento poderá ter o estado processado ou erro, caso o campo valor chegue vazio, o status do evento será erro caso contrário processado.

* Para visualização desses dados, sua solução deve possuir:
    * Uma tabela que mostre todos os eventos recebidos. Essa tabela deve ser atualizada automaticamente.
    * Um gráfico apenas para eventos com valor numérico.

* Para seu cliente, é muito importante que ele saiba o número de eventos que aconteceram por região e por sensor. Como no exemplo abaixo:
    * Região sudeste e sul ambas com dois sensores (sensor01 e sensor02):
        * brasil.sudeste - 1000
        * brasil.sudeste.sensor01 - 700
        * brasil.sudeste.sensor02 - 300
        * brasil.sul - 1500
        * brasil.sul.sensor01 - 1250
        * brasil.sul.sensor02 - 250

## Avaliação

Nossa equipe de desenvolvedores irá avaliar código, simplicidade da solução, testes unitários, arquitetura e automatização de tarefas.

Tente automatizar ao máximo sua solução. Isso porque no caso de deploy em vários servidores, não é interessante que tenhamos que entrar de máquina em máquina para instalar cada componente da solução.

Em caso de dúvida, entre em contato com o responsável pelo seu processo seletivo.

## Considerações do candidato

O sistema foi desenvolvido dividindo em front e back end. Ambos foram feitos em estrutura dotnet core, usando a versão 3.1.401, mas o front end poderia ser apenas um html, pois utilizei apenas javascript para fazer leitura dos dados e para plota-los em tela.

Dado o problema é possível fazer muito mais análises dos dados, uma vez que a entrada seja de eventos por sensores, é possível definir gráficos de intervalo de tempo de entrada, quais sensores mais mandam resultados, quais são os maiores resultados, entre outras propostas.

O sistema foi primeiramente testado com o postman, testando toda a parte de backend. O front apenas captura essas informações (GET), então apenas o teste unitário testa as outras requisições. Fiz o projeto do teste unitário no próprio visual code, mas houve muitos problemas em minha máquina, por falta de tempo acabei removendo o projeto de testes.

Para geração de mais dados usei ainda um aplicativo pessoal que desenvolvi a tempos, que monta um json genérico e dá valores aleatórios aos X campos que forem necessários possuir no arquivo (confessoq ue tive que modificar o aplicativo para gerar um timestamp aleatório).