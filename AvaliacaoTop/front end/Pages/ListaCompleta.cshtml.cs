﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace front_end.Pages
{
    public class ListaCompleta : PageModel
    {
        private readonly ILogger<ListaCompleta> _logger;

        public ListaCompleta(ILogger<ListaCompleta> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
        }
    }
}
