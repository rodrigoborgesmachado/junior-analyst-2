using Microsoft.EntityFrameworkCore;

namespace AvaliacaoTop.Models
{
    public class EventoContext : DbContext
    {
        public EventoContext(DbContextOptions<EventoContext> options)
            : base(options)
        {
        }

        public DbSet<EventoItem> EventoItems { get; set; }
    }
}