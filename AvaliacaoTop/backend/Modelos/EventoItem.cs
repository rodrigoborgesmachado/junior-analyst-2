public class EventoItem
{
    public long Id { get; set; }
    
    public string timeStamp { get; set; }

    public string tag { get; set; }

    public string valor { get; set; }

    public string status { get; set; }
}