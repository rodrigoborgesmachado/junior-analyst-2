using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AvaliacaoTop.Models;

namespace AvaliacaoTop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventoItemsController : ControllerBase
    {
        private readonly EventoContext _context;

        public EventoItemsController(EventoContext context)
        {
            _context = context;
        }

        // GET: api/EventoItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<EventoItem>>> GetEventosItems()
        {
            return await _context.EventoItems.ToListAsync();
        }

        // GET: api/EventoItems/1
        [HttpGet("{id}")]
        public async Task<ActionResult<EventoItem>> GetEventoItem(long id)
        {
            var eventoItem = await _context.EventoItems.FindAsync(id);

            if (eventoItem == null)
            {
                return NotFound();
            }

            return eventoItem;
        }

        // PUT: api/EventoItems/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEventoItem(long id, EventoItem eventoItem)
        {
            if (id != eventoItem.Id)
            {
                return BadRequest();
            }

            _context.Entry(eventoItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EventoItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/EventoItems
        [HttpPost]
        public async Task<ActionResult<EventoItem>> PostEventoItem(EventoItem eventoItem)
        {
            var evento = EventoItemExists(eventoItem);
            eventoItem.status = string.IsNullOrEmpty(eventoItem.valor) ? "erro" : "processado";

            if(evento == null){
                _context.EventoItems.Add(eventoItem);
                await _context.SaveChangesAsync();
                evento = eventoItem;
            }
            else{
                _context.Entry(evento).State = EntityState.Modified;
                
                evento.tag = eventoItem.tag;
                evento.timeStamp = eventoItem.timeStamp;
                evento.valor = eventoItem.valor;
                evento.status = eventoItem.status;

                await _context.SaveChangesAsync();
            }

            return CreatedAtAction(nameof(GetEventoItem), new { id = evento.Id }, evento);
        }

        // DELETE: api/EventoItems/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<EventoItem>> DeleteEventoItem(long id)
        {
            var eventoItem = await _context.EventoItems.FindAsync(id);
            if (eventoItem == null)
            {
                return NotFound();
            }

            _context.EventoItems.Remove(eventoItem);
            await _context.SaveChangesAsync();

            return eventoItem;
        }

        private bool EventoItemExists(long id)
        {
            return _context.EventoItems.Any(e => e.Id == id);
        }

        private EventoItem EventoItemExists(EventoItem element)
        {
            return _context.EventoItems.Where(e => e.tag == element.tag && e.valor == element.valor && e.timeStamp == element.timeStamp).FirstOrDefault();
        }
    }
}
